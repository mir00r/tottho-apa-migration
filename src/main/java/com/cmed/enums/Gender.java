package com.cmed.enums;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public enum Gender {

    MALE((byte) 1, "m"), FEMALE((byte) 2, "f"), OTHER((byte) 3, "o"), UNKNOWN((byte) 4, "u");

    private final Byte key;
    private final String value;

    Gender(Byte key, String value) {
        this.key = key;
        this.value = value;
    }

    public static Gender get(Byte id) {
        for (Gender type : Gender.values()) {
            if (type.key.equals(id)) return type;
        }
        return OTHER;
    }

    public static Gender get(String name) {
        for (Gender type : Gender.values()) {
            if (type.name().equals(name) || type.getValue().equals(name)) return type;
        }
        return OTHER;
    }

    public static boolean validate(Byte id) {
        for (Gender type : Gender.values()) {
            if (type.key.equals(id)) return true;
        }
        return false;
    }

    public static boolean validate(String name) {
        for (Gender type : Gender.values()) {
            if (type.name().equals(name) || type.getValue().contains(name)) return true;
        }
        return false;
    }

    public static Byte getKey(Byte id) {
        if (id == null) return OTHER.getKey();
        Gender type = get(id);
        if (type != null) return type.getKey();
        return OTHER.getKey();
    }

    public static Byte getKey(String name) {
        if (name == null) return OTHER.getKey();
        Gender type = get(name);
        if (type != null) return type.getKey();
        return OTHER.getKey();
    }

    public static String getValue(Byte id) {
        if (id == null) return OTHER.getValue();
        Gender type = get(id);
        if (type != null) return type.getValue();
        return OTHER.getValue();
    }

    public static String getValue(String name) {
        if (name == null) return OTHER.getValue();
        Gender type = get(name);
        if (type != null) return type.getValue();
        return OTHER.getValue();
    }

    public Byte getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
