package com.cmed.enums;

/**
 * @author mir00r on 28/10/20
 * @project IntelliJ IDEA
 */
public enum MeasurementTags {

    BEFORE_DOWN((byte) 1, "Before Down"),
    BEFORE_BREAKFAST((byte) 2, "Before Breakfast"),
    AFTER_BREAKFAST((byte) 3, "After Breakfast"),
    BEFORE_LUNCH((byte) 4, "Before Lunch"),
    AFTER_LUNCH((byte) 5, "After Lunch"),
    BEFORE_DINNER((byte) 6, "Before Dinner"),
    AFTER_DINNER((byte) 7, "After Dinner"),
    BEFORE_SLEEP((byte) 8, "Before Sleep"),
    RANDOM((byte) 9, "Random"),
    FASTING((byte) 10, "Fasting"),
    OGTT((byte) 11, "OGTT");

    private final Byte key;
    private final String value;

    MeasurementTags(Byte key, String value) {
        this.key = key;
        this.value = value;
    }

    public static MeasurementTags get(Byte id) {
        for (MeasurementTags type : MeasurementTags.values()) {
            if (type.key.equals(id)) return type;
        }
        return OGTT;
    }

    public static MeasurementTags get(String name) {
        for (MeasurementTags type : MeasurementTags.values()) {
            if (type.name().contains(name) || type.getValue().contains(name)) return type;
        }
        return OGTT;
    }

    public static boolean validate(Byte id) {
        for (MeasurementTags type : MeasurementTags.values()) {
            if (type.key.equals(id)) return true;
        }
        return false;
    }

    public static boolean validate(String name) {
        for (MeasurementTags type : MeasurementTags.values()) {
            if (type.name().equals(name) || type.getValue().contains(name)) return true;
        }
        return false;
    }

    public static Byte getKey(Byte id) {
        if (id == null) return OGTT.getKey();
        MeasurementTags type = get(id);
        if (type != null) return type.getKey();
        return OGTT.getKey();
    }

    public static Byte getKey(String name) {
        if (name == null) return OGTT.getKey();
        MeasurementTags type = get(name);
        if (type != null) return type.getKey();
        return OGTT.getKey();
    }

    public static String getValue(Byte id) {
        if (id == null) return OGTT.getValue();
        MeasurementTags type = get(id);
        if (type != null) return type.getValue();
        return OGTT.getValue();
    }

    public static String getName(Byte id) {
        if (id == null) return OGTT.name();
        MeasurementTags type = get(id);
        if (type != null) return type.name();
        return OGTT.name();
    }

    public static String getValue(String name) {
        if (name == null) return OGTT.getValue();
        MeasurementTags type = get(name);
        if (type != null) return type.getValue();
        return OGTT.getValue();
    }

    public static String getName(String name) {
        if (name == null) return OGTT.name();
        MeasurementTags type = get(name);
        if (type != null) return type.name();
        return OGTT.name();
    }
    public Byte getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
