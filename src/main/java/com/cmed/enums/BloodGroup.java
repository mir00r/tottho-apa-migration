package com.cmed.enums;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public enum BloodGroup {

    A_POSITIVE((byte) 1, "A+"),
    A_NEGATIVE((byte) 2, "A-"),
    B_POSITIVE((byte) 3, "B+"),
    B_NEGATIVE((byte) 4, "B-"),
    AB_POSITIVE((byte) 5, "AB+"),
    AB_NEGATIVE((byte) 6, "AB-"),
    O_POSITIVE((byte) 7, "O+"),
    O_NEGATIVE((byte) 8, "O-"),
    UNKNOWN((byte) 9, "Unknown");

    private final Byte key;
    private final String value;

    BloodGroup(Byte key, String value) {
        this.key = key;
        this.value = value;
    }

    public static BloodGroup get(Byte id) {
        for (BloodGroup type : BloodGroup.values()) {
            if (type.key.equals(id)) return type;
        }
        return UNKNOWN;
    }

    public static BloodGroup get(String name) {
        for (BloodGroup type : BloodGroup.values()) {
            if (type.name().contains(name) || type.getValue().contains(name)) return type;
        }
        return UNKNOWN;
    }

    public static boolean validate(Byte id) {
        for (BloodGroup type : BloodGroup.values()) {
            if (type.key.equals(id)) return true;
        }
        return false;
    }

    public static boolean validate(String name) {
        for (BloodGroup type : BloodGroup.values()) {
            if (type.name().equals(name) || type.getValue().contains(name)) return true;
        }
        return false;
    }

    public static Byte getKey(Byte id) {
        if (id == null) return UNKNOWN.getKey();
        BloodGroup type = get(id);
        if (type != null) return type.getKey();
        return UNKNOWN.getKey();
    }

    public static Byte getKey(String name) {
        if (name == null) return UNKNOWN.getKey();
        BloodGroup type = get(name);
        if (type != null) return type.getKey();
        return UNKNOWN.getKey();
    }

    public static String getValue(Byte id) {
        if (id == null) return UNKNOWN.getValue();
        BloodGroup type = get(id);
        if (type != null) return type.getValue();
        return UNKNOWN.getValue();
    }

    public static String getValue(String name) {
        if (name == null) return UNKNOWN.getValue();
        BloodGroup type = get(name);
        if (type != null) return type.getValue();
        return UNKNOWN.getValue();
    }

    public Byte getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
