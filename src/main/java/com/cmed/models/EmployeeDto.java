package com.cmed.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public class EmployeeDto {

    @JsonProperty("username")
    private String username;

    @JsonProperty("first_name_en")
    private String firstNameEn;

    @JsonProperty("last_name_en")
    private String lastNameEn;

    @JsonProperty("birthday")
    private Long birthday;

    @JsonProperty("height")
    private Double height;

    @JsonProperty("weight")
    private Double weight;

    @JsonProperty("diabetic")
    private Boolean diabetic;

    @JsonProperty("company_id")
    private Long companyId;

    @JsonProperty("company_employee_id")
    private String companyEmployeeId;

    @JsonProperty("gender")
    private Byte gender;

    @JsonProperty("blood_group")
    private Byte bloodGroup;

    @JsonProperty("phone")
    private String phone;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstNameEn() {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn) {
        this.firstNameEn = firstNameEn;
    }

    public String getLastNameEn() {
        return lastNameEn;
    }

    public void setLastNameEn(String lastNameEn) {
        this.lastNameEn = lastNameEn;
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Boolean getDiabetic() {
        return diabetic;
    }

    public void setDiabetic(Boolean diabetic) {
        this.diabetic = diabetic;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyEmployeeId() {
        return companyEmployeeId;
    }

    public void setCompanyEmployeeId(String companyEmployeeId) {
        this.companyEmployeeId = companyEmployeeId;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Byte getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(Byte bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
