package com.cmed.models;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public class MeasurementData {

    private Long measuredAt;
    private Long measurementTypeId;
    private Double value1;
    private Double value2;
    private Double value3;
    private String cmedId;
    private Long birthday;
    private String gender;
    private String bloodGroup;
    private String firstName;
    private String lastName;
    private Double height;
    private Double weight;
    private String phone;
    private Long userId;
    private Long tagId;

    public MeasurementData(Long measuredAt, Long measurementTypeId, Double value1,
                           Double value2, Double value3,
                           String cmedId, Long birthday,
                           String gender, String bloodGroup,
                           String firstName, String lastName,
                           Double height, Double weight,
                           String phone, Long tagId) {

        this.measuredAt = measuredAt;
        this.measurementTypeId = measurementTypeId;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.cmedId = cmedId;
        this.birthday = birthday;
        this.gender = gender;
        this.bloodGroup = bloodGroup;
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.weight = weight;
        this.phone = phone;
        this.tagId = tagId;
    }

    public MeasurementData(Long measuredAt, Long measurementTypeId, Double value1, Double value2, Double value3, Long tagId) {
        this.measuredAt = measuredAt;
        this.measurementTypeId = measurementTypeId;
        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.tagId = tagId;
    }

    public Long getMeasuredAt() {
        return measuredAt;
    }

    public void setMeasuredAt(Long measuredAt) {
        this.measuredAt = measuredAt;
    }

    public Long getMeasurementTypeId() {
        return measurementTypeId;
    }

    public void setMeasurementTypeId(Long measurementTypeId) {
        this.measurementTypeId = measurementTypeId;
    }

    public Double getValue1() {
        return value1;
    }

    public void setValue1(Double value1) {
        this.value1 = value1;
    }

    public Double getValue2() {
        return value2;
    }

    public void setValue2(Double value2) {
        this.value2 = value2;
    }

    public Double getValue3() {
        return value3;
    }

    public void setValue3(Double value3) {
        this.value3 = value3;
    }

    public String getCmedId() {
        return cmedId;
    }

    public void setCmedId(String cmedId) {
        this.cmedId = cmedId;
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getHeight() {
        if (height == null || height == 0 || height == 0.0) return 1.0;
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        if (weight == null || weight == 0.0) return 1.0;
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
}
