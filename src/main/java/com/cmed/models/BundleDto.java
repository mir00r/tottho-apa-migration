package com.cmed.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author mir00r on 28/10/20
 * @project IntelliJ IDEA
 */
public class BundleDto {

    @JsonProperty("bundles_measurements")
    private List<MeasurementDto> bundleMeasurement;

    @JsonProperty("user_id")
    private Long userId;

    public List<MeasurementDto> getBundleMeasurement() {
        return bundleMeasurement;
    }

    public void setBundleMeasurement(List<MeasurementDto> bundleMeasurement) {
        this.bundleMeasurement = bundleMeasurement;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
