package com.cmed.models;

/**
 * @author mir00r on 3/11/20
 * @project IntelliJ IDEA
 */
public class UserIdInfo {
    private Long sourceId;
    private Long destinationId;
    private Long sourceCompanyId;
    private Long destinationCompanyId;



    public UserIdInfo(Long sourceId) {
        this.sourceId = sourceId;
    }

    public UserIdInfo(Long sourceId, Long destinationId) {
        this.sourceId = sourceId;
        this.destinationId = destinationId;
    }

    public UserIdInfo(Long sourceId, Long destinationId, Long sourceCompanyId) {
        this.sourceId = sourceId;
        this.destinationId = destinationId;
        this.sourceCompanyId = sourceCompanyId;
    }

    public UserIdInfo(Long sourceId, Long destinationId, Long sourceCompanyId, Long destinationCompanyId) {
        this.sourceId = sourceId;
        this.destinationId = destinationId;
        this.sourceCompanyId = sourceCompanyId;
        this.destinationCompanyId = destinationCompanyId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }


    public Long getSourceCompanyId() {
        return sourceCompanyId;
    }

    public void setSourceCompanyId(Long sourceCompanyId) {
        this.sourceCompanyId = sourceCompanyId;
    }

    public Long getDestinationCompanyId() {
        return destinationCompanyId;
    }

    public void setDestinationCompanyId(Long destinationCompanyId) {
        this.destinationCompanyId = destinationCompanyId;
    }
}
