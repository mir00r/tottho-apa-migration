package com.cmed.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public class MeasurementDto {

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("measurement_type_code_id")
    private Byte measurementTypeCodeId;

    @JsonProperty("measured_at")
    private Long measuredAt;

    private Map<String, Double> inputs;

    private String tag;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Byte getMeasurementTypeCodeId() {
        return measurementTypeCodeId;
    }

    public void setMeasurementTypeCodeId(Byte measurementTypeCodeId) {
        this.measurementTypeCodeId = measurementTypeCodeId;
    }

    public Long getMeasuredAt() {
        return measuredAt;
    }

    public void setMeasuredAt(Long measuredAt) {
        this.measuredAt = measuredAt;
    }

    public Map<String, Double> getInputs() {
        return inputs;
    }

    public void setInputs(Map<String, Double> inputs) {
        this.inputs = inputs;
    }

    public String getTag() {
//        if (tag == null) return "";
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
