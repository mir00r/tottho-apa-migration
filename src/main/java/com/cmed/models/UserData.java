package com.cmed.models;

/**
 * @author mir00r on 28/10/20
 * @project IntelliJ IDEA
 */
public class UserData {
    private String cmedId;
    private Long companyId;
    private Long customerId;
    private Long birthday;
    private String gender;
    private String bloodGroup;
    private String firstName;
    private String lastName;
    private String username;
    private Double height;
    private Double weight;
    private String phone;
    private Long userId;

    public UserData(String cmedId, Long userId) {
        this.cmedId = cmedId;
        this.userId = userId;
    }

    public UserData(String cmedId, String username) {
        this.cmedId = cmedId;
        this.username = username;
    }

    public UserData(String cmedId, Long companyId, Long customerId) {
        this.cmedId = cmedId;
        this.companyId = companyId;
        this.customerId = customerId;
    }

    public UserData(String cmedId, Long companyId, Long customerId,
                    Long birthday, String gender,
                    String bloodGroup, String firstName,
                    String lastName, Double height,
                    Double weight, String phone) {
        this.cmedId = cmedId;
        this.companyId = companyId;
        this.customerId = customerId;
        this.birthday = birthday;
        this.gender = gender;
        this.bloodGroup = bloodGroup;
        this.firstName = firstName;
        this.lastName = lastName;
        this.height = height;
        this.weight = weight;
        this.phone = phone;
    }

    public String getCmedId() {
        return cmedId;
    }

    public void setCmedId(String cmedId) {
        this.cmedId = cmedId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public String getFormattedCustomerId() {
        if (customerId == null) return "";
        return String.valueOf(customerId);
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getHeight() {
        if (height == null || height == 0.0) return 1.0;
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        if (weight == null || weight == 0.0) return 1.0;
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getUserId() {
        if (userId == null) return 0L;
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
