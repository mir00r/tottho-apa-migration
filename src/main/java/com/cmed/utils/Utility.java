package com.cmed.utils;

import com.cmed.models.LoginResponseDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author mir00r on 28/10/20
 * @project IntelliJ IDEA
 */
public class Utility {

    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9_@\\.-]{4,}$";

    // strict regex
//    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){4,}[a-zA-Z0-9]$";

    private static final Pattern pattern = Pattern.compile(USERNAME_PATTERN);

    public static boolean isUserNameValid(final String username) {
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }

    public static String getFormattedPhoneNumber(String phone) {
        if (phone.startsWith("+88")) return phone.replace("+88", "").trim();
        return phone.trim();
    }

    public static LoginResponseDto getLoginResponse(String username, String password) throws IOException {
        String response = NetworkUtil.getResponseFromPostData(Constants.getLoginUrl(username, password), null, null);
        ObjectMapper mapper = new ObjectMapper();

        //map json to login dto class
        try {
            return mapper.readValue(response, LoginResponseDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long printTime(long timeStamp, String message) {
        System.out.println(message + new Date(timeStamp));
        return timeStamp;
    }

    public static void printTimeDifferance(long start, long end) {
        Date firstDate = new Date(start);
        Date secondDate = new Date(end);
        long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
        long diffInSecond = TimeUnit.SECONDS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        long diffInHours = TimeUnit.HOURS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        System.out.println("Time differance in second -> " + diffInSecond);
        System.out.println("Time differance in hours -> " + diffInHours);
    }

    private void writeCSVDataIntoFile(String fileName, String[] fields, List<String[]> data) throws IOException {
        CSVWriter writer = new CSVWriter(new FileWriter(fileName));
        writer.writeNext(fields);
        for (String[] datum : data) {
            writer.writeNext(datum);
        }
        writer.close();
        buildFile(fileName);
    }

    public static boolean isFileExist(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    public static String getFilePath(String fileName) {
        File file = new File(fileName);
        if (file.exists()) return file.getAbsolutePath();
        return fileName;
    }

    public static void buildFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static CSVWriter getCSVWriter(String fileName, String[] fields) throws IOException {
        CSVWriter writer;
        if (isFileExist(fileName)) {
            writer = new CSVWriter(new FileWriter(Objects.requireNonNull(getFilePath(fileName)), true));
        } else {
            writer = new CSVWriter(new FileWriter(fileName));
            writer.writeNext(fields);
        }
        return writer;
    }
}
