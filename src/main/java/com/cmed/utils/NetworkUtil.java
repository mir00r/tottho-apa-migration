package com.cmed.utils;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public class NetworkUtil {

//    public static String getClientIP() {
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
//                .getRequest();
//        String xfHeader = request.getHeader("X-Forwarded-For");
//        if (xfHeader == null) {
//            return request.getRemoteAddr();
//        }
//        return xfHeader.split(",")[0];
//    }

    public static void postData(String url, String body, String authorization) throws IOException {
        //new Thread(() -> {

        CloseableHttpClient client = HttpClients.createDefault();
        String newUrl = url.replace(" ", "%20");
        HttpPost httpPost = new HttpPost(newUrl);

        if (body != null && !body.isEmpty()) {
            StringEntity entity = new StringEntity(body, "UTF-8");
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Authorization", authorization);
            System.out.println("\n-------------------------------- Request Body for URL -> " + url + " -----------------------------------------------\n");
            System.out.println(EntityUtils.toString(entity));
        }
        try {
            CloseableHttpResponse response = client.execute(httpPost);
            System.out.println(response);
            System.out.println("\n-------------------------------- Response for URL -> " + url + " -----------------------------------------------\n");
            System.out.println(EntityUtils.toString(response.getEntity()));
            client.close();
            response.close();
        } catch (IOException ignored) {

        }
        //}).start();
    }

    public static String getResponseFromPostData(String url, String body, String authorization) throws IOException {
        String jsonString = null;
        CloseableHttpClient client = HttpClients.createDefault();
        String newUrl = url.replace(" ", "%20");

        HttpPost httpPost = new HttpPost(newUrl);
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(Constants.TIMEOUT_MILLIS)
                .setConnectTimeout(Constants.TIMEOUT_MILLIS)
                .setConnectionRequestTimeout(Constants.TIMEOUT_MILLIS)
                .build();
        httpPost.setConfig(requestConfig);

        if (body != null && !body.isEmpty()) {
            StringEntity entity = new StringEntity(body, "UTF-8");
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Authorization", authorization);
            System.out.println("\n-------------------------------- Request Body for URL -> " + url + " -----------------------------------------------\n");
            System.out.println(EntityUtils.toString(entity));
        }

        CloseableHttpResponse response = client.execute(httpPost);
        try {
//            jsonString = EntityUtils.toString(response.getEntity());
            System.out.println("\n-------------------------------- Response for URL -> " + url + " -----------------------------------------------\n");
            System.out.println(response);
//            System.out.println(EntityUtils.toString(response.getEntity()));
            return EntityUtils.toString(response.getEntity());
        } catch (IOException ignored) {

        } finally {
            client.close();
            response.close();
        }
        return jsonString;
    }

    public static String getResponseFromGETRequest(String url, String authorization) throws IOException {

        String jsonString = null;
        CloseableHttpClient client = HttpClients.createDefault();
        String newUrl = url.replace(" ", "%20");
        HttpGet httpGet = new HttpGet(newUrl);

        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("Authorization", authorization);

        try {
            CloseableHttpResponse response = client.execute(httpGet);
            jsonString = EntityUtils.toString(response.getEntity());
            System.out.println(response);
            System.out.println("\n-------------------------------- Response for URL -> " + url + " -----------------------------------------------\n");
            System.out.println(jsonString);
            client.close();
            response.close();
        } catch (IOException ignored) {

        }
        return jsonString;
    }

    public static void getRequest(String url, String body, String authorization) throws IOException {
        new Thread(() -> {

            CloseableHttpClient client = HttpClients.createDefault();
            String newUrl = url.replace(" ", "%20");
            HttpGet httpGet = new HttpGet(newUrl);

            if (body != null && !body.isEmpty()) {
                httpGet.setHeader("Accept", "application/json");
                httpGet.setHeader("Content-type", "application/json");
                httpGet.setHeader("Authorization", authorization);
            }
            try {
                CloseableHttpResponse response = client.execute(httpGet);
                client.close();
                response.close();
            } catch (IOException ignored) {

            }

        }).start();
    }

//    public static String responseBody(CloseableHttpResponse response) throws IOException {
//        try (InputStream is = response.getEntity() == null ? new NullInputStream(0) : response.getEntity().getContent()) {
//            String value = IOUtils.toString(is, StandardCharsets.UTF_8);
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.enable(SerializationFeature.INDENT_OUTPUT);
//            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(value);
//        }
//    }
//
//    public static Object getValueFromJsonArray(JSONArray array, String key) {
//        Object value = null;
//        for (int i = 0; i < array.length(); i++) {
//            JSONObject item = array.getJSONObject(i);
//            if (item.keySet().contains(key)) {
//                value = item.get(key);
//                break;
//            }
//        }
//        return value;
//    }
}
