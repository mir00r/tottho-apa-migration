package com.cmed.utils;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public class Constants {

    public static final String LIMIT = "1000";
    public static final int MEASUREMENT_LIMIT = 300;
    public static final int TIMEOUT_MILLIS = 240000; // Four minute

    // Source Database Credentials
    public static final String DB_URL = "jdbc:mysql://localhost:3306/tottho_apa_prod_src?useSSL=false";
    public static final String DB_USER_NAME = "root";
    public static final String DB_PASSWORD = null;

    // Destination
    public static final String DB_DESTINATION_URL = "jdbc:mysql://localhost:3306/tottho_apa_prod_dest?allowPublicKeyRetrieval=true&useSSL=false";
    public static final String DB_DESTINATION_USER_NAME = "root";
    public static final String DB_DESTINATION_PASSWORD = null;

    // Destination Database Credentials
//    public static final String CMED_BASE_URL = "http://cmed-core-dev.cmedhealth.com";
//    public static final String CMED_BASE_URL = "http://cmed-core-staging.cmedhealth.com";
//    public static final String CMED_BASE_URL = "http://http://cmed-core-prod.cmedhealth.com";
    public static final String CMED_BASE_URL = "http://localhost:9988";
    //    public static final String CMED_BASE_URL = "http://migration-test.cmedhealth.com";
//    public static final String CMED_BASE_URL = "https://totthoapav2.cmedhealth.com";
    public static final String OAUTH_URL = "/oauth/token?grant_type=password&client_id=client_id&client_secret=client_secret";
    public static final String CMED_USER_NAME = "&username=";
    public static final String CMED_PASSWORD = "&password=";

    // Destination system credentials
    public static final String DESTINATION_USERNAME = "totthoapa_admin";
    public static final String DESTINATION_PASSWORD = "totthoapa_admin";
//    public static final String DESTINATION_PASSWORD = "totthoapaadmin";

//    public static final String DESTINATION_USERNAME = "admin";
//    public static final String DESTINATION_PASSWORD = "password";

    // API URLS
    public static final String CMED_USER_BY_USERNAME_URL = "/api/v6/users/";
    public static final String CMED_AGENT_ME_URL = "/api/v6/agents/me";
    public static final String COUNT_NOT_MEASURED_EMPLOYEES = "/api/v6/employees/not/measured/count";
    public static final String NOT_MEASURED_EMPLOYEES_INFO = "/api/v6/employees/not/measured/info";
    public static final String CMED_EMPLOYEE_POST_URL = "/api/v6/employees";
    public static final String CMED_EMPLOYEE_BULK_POST_URL = "/api/v6/employees/bulk";
    public static final String CMED_MEASUREMENT_POST_URL = "/api/v7/measurements/bulk";
    public static final String CMED_BUNDLES_BULK_POST_URL = "/api/v7/bundles/bulk";
    public static final String CMED_BUNDLES_POST_URL = "/api/v6/bundles";
    public static final String CMED_GET_MEASUREMENT_BY_USER_URL = "/api/v6/measurements";


    public static String getLoginUrl(String username, String password) {
        return CMED_BASE_URL + OAUTH_URL + CMED_USER_NAME + username + CMED_PASSWORD + password;
    }

    public static String getUserUrlByUserName(String username) {
        return CMED_BASE_URL + CMED_USER_BY_USERNAME_URL + username;
    }

    public static String getAgentMeUrl() {
        return CMED_BASE_URL + CMED_AGENT_ME_URL;
    }

    public static String getCountNotMeasuredEmployeeUrl(Long companyId, Long createdBy) {
        return CMED_BASE_URL + COUNT_NOT_MEASURED_EMPLOYEES + "?company_id=" + companyId + "&created_by_id=" + createdBy;
    }

    public static String getNotMeasuredEmployeeUrl(Long companyId, Long createdBy, int page, int size) {
        return CMED_BASE_URL + NOT_MEASURED_EMPLOYEES_INFO + "?company_id=" + companyId + "&created_by_id=" + createdBy + "&page=" + page + "&size=" + size;
    }

    public static String getMeasuredByUserUrl(Long userId, int page, int size) {
        return CMED_BASE_URL + CMED_GET_MEASUREMENT_BY_USER_URL + "?page=" + page + "&size=" + size + "&userId=" + userId;
    }

    public static String getEmployeePostUrl() {
        return CMED_BASE_URL + CMED_EMPLOYEE_POST_URL;
    }

    public static String getEmployeeBulkPostUrl() {
        return CMED_BASE_URL + CMED_EMPLOYEE_BULK_POST_URL;
    }

    public static String getMeasurementPostUrl() {
        return CMED_BASE_URL + CMED_MEASUREMENT_POST_URL;
    }

    public static String getBulkBundlesPostUrl() {
        return CMED_BASE_URL + CMED_BUNDLES_BULK_POST_URL;
    }

    public static String getBundlesPostUrl() {
        return CMED_BASE_URL + CMED_BUNDLES_POST_URL;
    }
}
