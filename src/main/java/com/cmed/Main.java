package com.cmed;

import com.cmed.services.MigrationService;
import com.cmed.services.MigrationServiceImpl;
import com.cmed.services.compare.DataCompareService;
import com.cmed.services.compare.DataCompareServiceImpl;
import com.cmed.services.measurements.MeasurementMigrationService;
import com.cmed.services.measurements.MeasurementMigrationServiceIml;
import com.cmed.services.users.UserMigrationService;
import com.cmed.services.users.UserMigrationServiceImpl;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        // Upload user and measurement
//        MigrationService migrationService = new MigrationServiceImpl();
//        migrationService.execute("TotthoApa");

        // Upload only user
//        UserMigrationService userMigrationService = new UserMigrationServiceImpl();
//        userMigrationService.execute("TotthoApa");

        // Upload only measurement with respective company user
        MeasurementMigrationService measurementMigrationService = new MeasurementMigrationServiceIml();
        measurementMigrationService.execute("TotthoApa");

//        DataCompareService dataCompareService = new DataCompareServiceImpl();
//        dataCompareService.compare("TotthoApa", 1L);
    }
}
