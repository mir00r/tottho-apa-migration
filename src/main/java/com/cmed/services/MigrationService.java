package com.cmed.services;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface MigrationService {
    void execute(String companyCode);
}
