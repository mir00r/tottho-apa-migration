package com.cmed.services.login;

import com.cmed.models.LoginResponseDto;

import java.io.IOException;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface LoginService {
    LoginResponseDto postLoginInDestination(String url, String username, String password) throws IOException;
}
