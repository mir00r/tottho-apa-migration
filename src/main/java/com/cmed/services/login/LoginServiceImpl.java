package com.cmed.services.login;

import com.cmed.models.LoginResponseDto;
import com.cmed.utils.Constants;
import com.cmed.utils.NetworkUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class LoginServiceImpl implements LoginService {

    @Override
    public LoginResponseDto postLoginInDestination(String url, String username, String password) throws IOException {
        String response = NetworkUtil.getResponseFromPostData(Constants.getLoginUrl(username, password), null, null);
        ObjectMapper mapper = new ObjectMapper();

        //map json to login dto class
        try {
            return mapper.readValue(response, LoginResponseDto.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
