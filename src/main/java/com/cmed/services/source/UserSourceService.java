package com.cmed.services.source;

import com.cmed.models.UserData;

import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface UserSourceService {
    List<UserData> getUserSourceData(String companyCode, String offset, String limit);
    Long getSourceUserCount(String companyCode);
    Long getSourceCompanyId(String companyCode);
}
