package com.cmed.services.source;

import com.cmed.Main;
import com.cmed.models.MeasurementData;
import com.cmed.utils.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class MeasurementSourceServiceImpl implements MeasurementSourceService {

    @Override
    public List<MeasurementData> getMeasurementSourceData(Long personId) {
        List<MeasurementData> measurementData = new ArrayList<>();

        String measurementQuery = "SELECT um.measured_at, um.measurement_type_id, um.value1, um.value2, um.value3, mt.tag_id " +
                "FROM user_measurement um " +
                "left join measurement_tags mt on um.id = mt.measurement_id " +
                "WHERE um.person_id = " + personId + " ORDER BY um.created_at DESC ";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(measurementQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                measurementData.add(new MeasurementData(
                        rs.getTimestamp(1).getTime(),
                        rs.getLong(2), rs.getDouble(3),
                        rs.getDouble(4), rs.getDouble(5),
                        rs.getLong(6)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return measurementData;
    }

    @Override
    public List<MeasurementData> getMeasurementSourceData(Long personId, Long companyId) {
        List<MeasurementData> measurementData = new ArrayList<>();

        String measurementQuery = "SELECT um.measured_at, um.measurement_type_id, um.value1, um.value2, um.value3, mt.tag_id " +
                "FROM user_measurement um " +
                "left join measurement_tags mt on um.id = mt.measurement_id " +
                "WHERE um.company_id = " + companyId + " AND um.person_id = " + personId + " ORDER BY um.created_at DESC ";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(measurementQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                measurementData.add(new MeasurementData(
                        rs.getTimestamp(1).getTime(),
                        rs.getLong(2), rs.getDouble(3),
                        rs.getDouble(4), rs.getDouble(5),
                        rs.getLong(6)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return measurementData;
    }

    @Override
    public List<MeasurementData> getFilteredMeasurementSourceData(Long personId, Long companyId) {
        List<MeasurementData> measurementData = new ArrayList<>();

        String measurementQuery = "SELECT um.measured_at, um.measurement_type_id, um.value1, um.value2, um.value3, mt.tag_id " +
                "FROM user_measurement um " +
                "left join measurement_tags mt on um.id = mt.measurement_id " +
                "WHERE um.company_id = " + companyId + " AND um.person_id = " + personId + " GROUP BY measurement_type_id, measured_at, value1, value2, value3, mt.tag_id ";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(measurementQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                measurementData.add(new MeasurementData(
                        rs.getTimestamp(1).getTime(),
                        rs.getLong(2), rs.getDouble(3),
                        rs.getDouble(4), rs.getDouble(5),
                        rs.getLong(6)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return measurementData;
    }
}
