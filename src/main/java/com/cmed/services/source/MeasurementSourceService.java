package com.cmed.services.source;

import com.cmed.models.MeasurementData;

import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface MeasurementSourceService {
    List<MeasurementData> getMeasurementSourceData(Long personId);

    List<MeasurementData> getMeasurementSourceData(Long personId, Long companyId);
    List<MeasurementData> getFilteredMeasurementSourceData(Long personId, Long companyId);
}
