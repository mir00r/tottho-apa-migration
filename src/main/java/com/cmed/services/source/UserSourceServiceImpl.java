package com.cmed.services.source;

import com.cmed.Main;
import com.cmed.models.UserData;
import com.cmed.utils.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class UserSourceServiceImpl implements UserSourceService {

    @Override
    public List<UserData> getUserSourceData(String companyCode, String offset, String limit) {
        List<UserData> sourceData = new ArrayList<>();

        String userQuery = "select u.cmed_id, u.company_id, u.customer_id, p.birthday, p.gender, p.blood_group, p.first_name, p.last_name, p.height, p.weight, p.phone_number from user u join person p on u.customer_id = p.id where company_id = " +
                "(SELECT c.id FROM company c WHERE code = '" + companyCode + "')" +
                " and cmed_id not like 'tsk%' limit " +
                Constants.LIMIT +
                " offset " +
                offset;
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                sourceData.add(new UserData(
                        rs.getString(1), rs.getLong(2), rs.getLong(3),
                        rs.getTimestamp(4).getTime(), rs.getString(5),
                        rs.getString(6), rs.getString(7),
                        rs.getString(8), rs.getDouble(9),
                        rs.getDouble(10), rs.getString(11)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return sourceData;
    }

    @Override
    public Long getSourceUserCount(String companyCode) {
        long userCount = 0L;
        String userQuery = "select count(*) from user u where company_id = " +
                "(SELECT c.id FROM company c WHERE code = '" + companyCode + "')" +
                " and cmed_id not like 'tsk%' ";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                userCount = rs.getLong(1);
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return userCount;
    }

    @Override
    public Long getSourceCompanyId(String companyCode) {
        long companyId = 0L;
        String userQuery = "SELECT c.id FROM company c WHERE code = '" + companyCode + "'";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                companyId = rs.getLong(1);
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return companyId;
    }
}
