package com.cmed.services.compare;

import com.cmed.models.UserData;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author mir00r on 5/1/21
 * @project IntelliJ IDEA
 */
public class DataCompareServiceImpl implements DataCompareService {

    private final SourceUserInfo sourceUserInfo;
    private final DestinationUserInfo destinationUserInfo;

    public DataCompareServiceImpl() {
        this.sourceUserInfo = new SourceUserInfoImpl();
        this.destinationUserInfo = new DestinationUserInfoImpl();
    }

    @Override
    public void compare(String sourceCompanyCode, Long destinationCompanyId) {
        List<UserData> sourceData = this.sourceUserInfo.getUserSourceData(sourceCompanyCode);
        System.out.println("Found total source user data -> " + sourceData.size());
        List<UserData> destinationData = this.destinationUserInfo.getUserSourceData(destinationCompanyId);
        System.out.println("Found total destination user data -> " + destinationData.size());

        List<String> listOne = sourceData.stream().map(UserData::getFormattedCustomerId).collect(Collectors.toList());
        List<String> listTwo = destinationData.stream().map(UserData::getCmedId).collect(Collectors.toList());

        Collection<String> similar = new HashSet<>(listOne);
        Collection<String> different = new HashSet<>();

        different.addAll(listOne);
        different.addAll(listTwo);

        similar.retainAll(listTwo);
        different.removeAll(similar);

        System.out.printf("Total Similar:%s", similar.size());
        System.out.println("\n-----------------------------------------------------------------------------------------\n");
//        System.out.printf("Similar:%s", similar);
        System.out.println("\n-----------------------------------------------------------------------------------------\n");
        System.out.printf("Total Different :%s%n", different.size());
        System.out.println("\n-----------------------------------------------------------------------------------------\n");
        System.out.printf("Different:%s%n", different);
    }
}
