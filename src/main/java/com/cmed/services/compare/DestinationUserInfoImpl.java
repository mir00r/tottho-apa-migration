package com.cmed.services.compare;

import com.cmed.Main;
import com.cmed.models.UserData;
import com.cmed.utils.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mir00r on 5/1/21
 * @project IntelliJ IDEA
 */
public class DestinationUserInfoImpl implements DestinationUserInfo {
    @Override
    public List<UserData> getUserSourceData(Long companyId) {
        List<UserData> sourceData = new ArrayList<>();

        String userQuery = "select e.employee_id, mu.username from employees e join m_users mu on mu.id = e.user_id where e.company_id =" + companyId;
        try (Connection con = DriverManager.getConnection(Constants.DB_DESTINATION_URL, Constants.DB_USER_NAME, Constants.DESTINATION_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                sourceData.add(new UserData(
                        rs.getString(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return sourceData;
    }
}
