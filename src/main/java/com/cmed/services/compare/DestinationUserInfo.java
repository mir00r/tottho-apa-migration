package com.cmed.services.compare;

import com.cmed.models.UserData;

import java.util.List;

/**
 * @author mir00r on 5/1/21
 * @project IntelliJ IDEA
 */
public interface DestinationUserInfo {
    List<UserData> getUserSourceData(Long companyId);
}
