package com.cmed.services.compare;

/**
 * @author mir00r on 5/1/21
 * @project IntelliJ IDEA
 */
public interface DataCompareService {
    void compare(String sourceCompanyCode, Long destinationCompanyId);
}
