package com.cmed.services.compare;

import com.cmed.Main;
import com.cmed.models.UserData;
import com.cmed.utils.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mir00r on 5/1/21
 * @project IntelliJ IDEA
 */
public class SourceUserInfoImpl implements SourceUserInfo{
    @Override
    public List<UserData> getUserSourceData(String companyCode) {
        List<UserData> sourceData = new ArrayList<>();

        String userQuery = "select u.cmed_id, u.company_id, u.customer_id from user u join person p on u.customer_id = p.id where company_id = " +
                "(SELECT c.id FROM company c WHERE code = '" + companyCode + "')" +
                " and cmed_id not like 'tsk%';";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                sourceData.add(new UserData(
                        rs.getString(1), rs.getLong(2), rs.getLong(3)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return sourceData;
    }
}
