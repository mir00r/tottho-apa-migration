package com.cmed.services.users;

import com.cmed.models.UserIdInfo;

import java.io.IOException;
import java.util.List;

/**
 * @author mir00r on 2/11/20
 * @project IntelliJ IDEA
 */
public interface UserMigrationService {
    List<UserIdInfo> execute(String companyCode) throws IOException;
}
