package com.cmed.services.users;

import com.cmed.models.LoginResponseDto;
import com.cmed.models.UserData;
import com.cmed.models.UserIdInfo;
import com.cmed.services.destination.UserDestinationService;
import com.cmed.services.destination.UserDestinationServiceImpl;
import com.cmed.services.login.LoginService;
import com.cmed.services.login.LoginServiceImpl;
import com.cmed.services.source.UserSourceService;
import com.cmed.services.source.UserSourceServiceImpl;
import com.cmed.utils.Constants;
import com.cmed.utils.NetworkUtil;
import com.cmed.utils.Utility;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mir00r on 2/11/20
 * @project IntelliJ IDEA
 */
public class UserMigrationServiceImpl implements UserMigrationService {

    private final LoginService loginService;

    private final UserSourceService userSourceService;
    private final UserDestinationService userDestinationService;

    public UserMigrationServiceImpl() {
        this.loginService = new LoginServiceImpl();
        this.userSourceService = new UserSourceServiceImpl();
        this.userDestinationService = new UserDestinationServiceImpl();
    }

    @Override
    public List<UserIdInfo> execute(String companyCode) throws IOException {
        List<UserIdInfo> responseUserIds = new ArrayList<>();

        System.out.println("\n ------------------ Start User submission --------------------- \n");
        long startTime = Utility.printTime(System.currentTimeMillis(), "Start Time -> ");
        try {
            int offset = 0;

            // Get login response from destination login api
            LoginResponseDto loginResponse = this.loginService.postLoginInDestination(null, Constants.DESTINATION_USERNAME, Constants.DESTINATION_PASSWORD);
            String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();
            Long destinationCompanyId = this.getDestinationCompanyId(token);


//            long userCount = 5;
            //  Get total user count from source database
            long userCount = this.userSourceService.getSourceUserCount(companyCode);
            while (userCount > 0) {
                // Get company user list from source database
                List<UserData> userData = this.userSourceService.getUserSourceData(companyCode, String.valueOf(offset), Constants.LIMIT);
                if (userData.size() == 0) break;
                offset += userData.size();
                userCount -= userData.size();

                // Post bulk user or employee into destination database through employee api and getting response
                responseUserIds = this.userDestinationService.postBulkUserToDestination(userData, loginResponse, destinationCompanyId);
            }
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        } finally {
            System.out.println("\n ------------------ User submission completed --------------------- \n");
            long endTime = Utility.printTime(System.currentTimeMillis(), "End Time -> ");
            Utility.printTimeDifferance(startTime, endTime);
        }
        return responseUserIds;
    }

    private Long getDestinationCompanyId(String token) throws IOException {
        String agentMe = NetworkUtil.getResponseFromGETRequest(Constants.getAgentMeUrl(), token);
        JSONObject agentObj = new JSONObject(agentMe);
        if (agentMe.startsWith("{")) {
            return agentObj.getLong("id");
        } else return 1L;
    }
}
