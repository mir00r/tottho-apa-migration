package com.cmed.services.measurements;

import com.cmed.models.*;
import com.cmed.services.csv.CsvFileWriterService;
import com.cmed.services.csv.CsvFileWriterServiceImpl;
import com.cmed.services.destination.BundleDestinationService;
import com.cmed.services.destination.BundleDestinationServiceImpl;
import com.cmed.services.destination.MeasurementDestinationService;
import com.cmed.services.destination.MeasurementDestinationServiceImpl;
import com.cmed.services.source.MeasurementSourceService;
import com.cmed.services.source.MeasurementSourceServiceImpl;
import com.cmed.utils.Constants;
import com.cmed.utils.NetworkUtil;
import com.cmed.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mir00r on 24/11/20
 * @project IntelliJ IDEA
 */
public class MeasurementPostServiceImpl implements MeasurementPostService {

    private final MeasurementSourceService measurementSourceService;

    private final MeasurementDestinationService measurementDestinationService;
    private final BundleDestinationService bundleDestinationService;

    private final CsvFileWriterService csvFileWriterService;

    public MeasurementPostServiceImpl() {
        this.measurementSourceService = new MeasurementSourceServiceImpl();
        this.measurementDestinationService = new MeasurementDestinationServiceImpl();
        this.bundleDestinationService = new BundleDestinationServiceImpl();
        this.csvFileWriterService = new CsvFileWriterServiceImpl();
    }

    @Override
    public void processMeasurement(String token, List<UserIdInfo> userIdList) throws IOException {

        System.out.println("\n ------------------ Start Measurement submission --------------------- \n");
        long startTime = Utility.printTime(System.currentTimeMillis(), "Start Time -> ");
        try {
            for (UserIdInfo uData : userIdList) {
                // Get measurement data list according to user or customer from source database
                List<MeasurementData> measurementData = this.measurementSourceService.getMeasurementSourceData(uData.getSourceId(), uData.getSourceCompanyId());
                System.out.println("Total measurement data found without filtering -> " + measurementData.size() + " for this person -> " + uData.getSourceId());

                System.out.println("\n ------------------ Source measurement data difference --------------------- \n");

                List<MeasurementData> filteredMeasurementData = this.measurementSourceService.getFilteredMeasurementSourceData(uData.getSourceId(), uData.getSourceCompanyId());
                System.out.println("Total measurement data found after filtering -> " + filteredMeasurementData.size() + " for this person -> " + uData.getSourceId());

                int mSize = filteredMeasurementData.size(); //measurementData.size();
                if (mSize > 0) {
                    long totalDestinationMeasurementCount = this.getDestinationMeasurementCount(token, uData.getDestinationId(), 0, 1);
                    if (totalDestinationMeasurementCount == mSize)
                        System.out.println("Already measurement uploaded of destination userId: " + uData.getDestinationId() + " and source personId: " + uData.getSourceId());
                    else {
                        List<BundleDto> bundleRequest = new ArrayList<>();
                        List<MeasurementDto> measurementDtoList = new ArrayList<>();

                        // prepare measurement data
                        for (MeasurementData mData : filteredMeasurementData) {
                            mData.setUserId(uData.getDestinationId());
                            measurementDtoList.add(this.measurementDestinationService.prepareMeasurementData(mData));
                        }

                        // Prepare bundle data
                        bundleRequest.add(this.bundleDestinationService.prepareBundleData(measurementDtoList, uData.getDestinationId()));
                        this.csvFileWriterService.writeBundleRequestToCSVFile(bundleRequest);

                        // Make bundle data into JSON format
                        ObjectMapper mapper = new ObjectMapper();
                        // Write request into CSV file
                        String bundleJson = mapper.writeValueAsString(bundleRequest);

//                    System.out.println("\n --------------------------- Posting bundle measurement data --------------------------- \n" + bundleJson);
                        // Post bundle data into destination database through API
                        String bundleResponse = NetworkUtil.getResponseFromPostData(Constants.getBulkBundlesPostUrl(), bundleJson, token);
                        if (bundleResponse.startsWith("{")) {
//                            response.add(new JSONObject(bundleResponse));
                            // Write response into CSV file
                            this.csvFileWriterService.writeBundleResponseToCSVFile(new JSONObject(bundleResponse));
                        }
                    }
//                    List<BundleDto> bundleRequest = new ArrayList<>();
//                    List<MeasurementDto> measurementDtoList = new ArrayList<>();

                    // Prepare measurement data
//                    int mSize = measurementData.size();
//                    int offset = 0;
//                    while (mSize > 0) {
//                        int limit = Math.min(mSize, Constants.MEASUREMENT_LIMIT);
//                        for (int i = offset; i < limit; i++) {
//                            MeasurementData mData = measurementData.get(i);
//                            mData.setUserId(uData.getDestinationId());
//                            measurementDtoList.add(this.measurementDestinationService.prepareMeasurementData(mData));
//                        }
//                        offset += limit;
//                        mSize -= limit;

//                    for (MeasurementData mData : measurementData) {
//                        mData.setUserId(uData.getDestinationId());
//                        measurementDtoList.add(this.measurementDestinationService.prepareMeasurementData(mData));
//                    }
//
//                    // Prepare bundle data
//                    bundleRequest.add(this.bundleDestinationService.prepareBundleData(measurementDtoList, uData.getDestinationId()));
//                    this.csvFileWriterService.writeBundleRequestToCSVFile(bundleRequest);
//
//                    // Make bundle data into JSON format
//                    ObjectMapper mapper = new ObjectMapper();
//                    // Write request into CSV file
//                    String bundleJson = mapper.writeValueAsString(bundleRequest);
//
////                    System.out.println("\n --------------------------- Posting bundle measurement data --------------------------- \n" + bundleJson);
//                    // Post bundle data into destination database through API
//                    String bundleResponse = NetworkUtil.getResponseFromPostData(Constants.getBulkBundlesPostUrl(), bundleJson, token);
//                    if (bundleResponse.startsWith("{")) {
////                            response.add(new JSONObject(bundleResponse));
//                        // Write response into CSV file
//                        this.csvFileWriterService.writeBundleResponseToCSVFile(new JSONObject(bundleResponse));
//                    }
//                    System.out.println("\n --------------------------- Response after successfully posting bundle measurement data --------------------------- \n" + bundleResponse);
//                    }
                } else System.out.println("Measurement data not found with person id: " + uData.getSourceId());
            }
        } catch (Exception exception) {
            System.out.println(exception.getLocalizedMessage());
        } finally {
            System.out.println("\n ------------------ Measurement submission completed --------------------- \n");
            long endTime = Utility.printTime(System.currentTimeMillis(), "End Time -> ");
            Utility.printTimeDifferance(startTime, endTime);
        }
    }

    private long getDestinationMeasurementCount(String token, Long userId, int page, int size) throws IOException {
        String measurementData = NetworkUtil.getResponseFromGETRequest(Constants.getMeasuredByUserUrl(userId, page, size), token);
        JSONObject jsonResponse = new JSONObject(measurementData);
        return jsonResponse.getLong("totalElements");
    }
}
