package com.cmed.services.measurements;

import com.cmed.models.LoginResponseDto;
import com.cmed.models.UserData;
import com.cmed.models.UserIdInfo;
import com.cmed.services.login.LoginService;
import com.cmed.services.login.LoginServiceImpl;
import com.cmed.services.source.UserSourceService;
import com.cmed.services.source.UserSourceServiceImpl;
import com.cmed.utils.Constants;
import com.cmed.utils.NetworkUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mir00r on 24/11/20
 * @project IntelliJ IDEA
 */
public class MeasurementMigrationServiceIml implements MeasurementMigrationService {

    private final LoginService loginService;
    private final UserMeasurementService userMeasurementService;
    private final UserSourceService userSourceService;
    private final MeasurementPostService measurementPostService;

    public MeasurementMigrationServiceIml() {
        this.loginService = new LoginServiceImpl();
        this.userMeasurementService = new UserMeasurementServiceImpl();
        this.userSourceService = new UserSourceServiceImpl();
        this.measurementPostService = new MeasurementPostServiceImpl();
    }

    @Override
    public void execute(String companyCode) throws IOException {
        // Get login response from destination login api
        LoginResponseDto loginResponse = this.loginService.postLoginInDestination(null, Constants.DESTINATION_USERNAME, Constants.DESTINATION_PASSWORD);
        // Formatted login token
        String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();

        // Get agent profile information from destination database
        String agentMe = NetworkUtil.getResponseFromGETRequest(Constants.getAgentMeUrl(), token);
        Long companyId = this.getCompanyId(agentMe);
        Long createdById = this.getCompanyUserId(agentMe);

        // Get company id number from source database
        Long sourceCompanyId = this.userSourceService.getSourceCompanyId(companyCode);

        int page = 0;
        int size = 10000000;
        // Get count user which is still not measured
//        long userCount = this.userMeasurementService.countUserNotMeasured(companyId, createdById);
        long userCount = this.getCount(token, companyId, createdById);
//        long userCount = 1;
        while (userCount > 0) {
            // Get user list data which is need to be measured
//            List<UserData> userData = this.userMeasurementService.getUserNotMeasured(companyId, createdById, String.valueOf(offset), Constants.LIMIT);
            List<UserData> userData = this.getUserData(token, companyId, createdById, page, size);
            if (userData.size() == 0) {
                break;
            }
            page += 1;
            userCount -= userData.size();

            List<UserIdInfo> userIdList = new ArrayList<>();
            for (UserData uData : userData) {
                userIdList.add(new UserIdInfo(Long.parseLong(uData.getCmedId()), uData.getUserId(), sourceCompanyId));
            }
            // Start to process user information to prepare measurement data
            this.measurementPostService.processMeasurement(token, userIdList);
        }
    }

    private long getCount(String token, Long companyId, Long createdBy) throws IOException {
        String userCount = NetworkUtil.getResponseFromGETRequest(Constants.getCountNotMeasuredEmployeeUrl(companyId, createdBy), token);
        return Long.parseLong(userCount);
    }

    private List<UserData> getUserData(String token, Long companyId, Long createdBy, int page, int size) throws IOException {
        String employeeData = NetworkUtil.getResponseFromGETRequest(Constants.getNotMeasuredEmployeeUrl(companyId, createdBy, page, size), token);
        JSONObject jsonResponse = new JSONObject(employeeData);

        JSONArray contentList;
        List<UserData> userData = new ArrayList<>();
        if (jsonResponse.getJSONArray("content").length() > 0) {
            contentList = jsonResponse.getJSONArray("content");
            for (int i = 0; i < contentList.length(); i++) {
                JSONObject employeeResponse = contentList.getJSONObject(i);
                userData.add(new UserData(employeeResponse.getString("company_employee_id"), employeeResponse.getLong("user_id")));
            }
        }
        return userData;
    }

    private Long getCompanyId(String agentMe) {
        JSONObject agentObj = new JSONObject(agentMe);
        if (agentMe.startsWith("{")) {
            return agentObj.getLong("id");
        } else return 11L;
    }

    private Long getCompanyUserId(String agentMe) {
        JSONObject agentObj = new JSONObject(agentMe);
        if (agentMe.startsWith("{")) {
            return agentObj.getLong("user_id");
        } else return 14L;
    }
}
