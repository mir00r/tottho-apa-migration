package com.cmed.services.measurements;

import com.cmed.models.UserData;

import java.util.List;

/**
 * @author mir00r on 24/11/20
 * @project IntelliJ IDEA
 */
public interface UserMeasurementService {
    List<UserData> getUserNotMeasured(Long companyId, Long createdById, String offset, String limit);
    Long countUserNotMeasured(Long companyId, Long createdById);
}
