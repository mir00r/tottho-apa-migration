package com.cmed.services.measurements;

import com.cmed.models.UserIdInfo;

import java.io.IOException;
import java.util.List;

/**
 * @author mir00r on 24/11/20
 * @project IntelliJ IDEA
 */
public interface MeasurementPostService {
    void processMeasurement(String token, List<UserIdInfo> userIdList) throws IOException;
}
