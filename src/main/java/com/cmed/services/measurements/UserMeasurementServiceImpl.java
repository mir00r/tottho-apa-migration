package com.cmed.services.measurements;

import com.cmed.Main;
import com.cmed.models.UserData;
import com.cmed.utils.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mir00r on 24/11/20
 * @project IntelliJ IDEA
 */
public class UserMeasurementServiceImpl implements UserMeasurementService {

    @Override
    public List<UserData> getUserNotMeasured(Long companyId, Long createdById, String offset, String limit) {
        List<UserData> userData = new ArrayList<>();

        String userQuery = "select e.* from employees e where e.company_id = " + companyId + " and e.created_by_id = " + createdById + " and e.user_id not in (select m.user_id from measurements m where m.created_by_id = " + createdById + " and m.deleted=false) limit " +
                limit +
                " offset " +
                offset;
        try (Connection con = DriverManager.getConnection(Constants.DB_DESTINATION_URL, Constants.DB_DESTINATION_USER_NAME, Constants.DB_DESTINATION_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                userData.add(new UserData(rs.getString(8), rs.getLong(13)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return userData;

    }

    @Override
    public Long countUserNotMeasured(Long companyId, Long createdById) {
        long userCount = 0L;
        String userQuery = "select count(*) from employees e where e.company_id = " + companyId + " and e.created_by_id = " + createdById + " and e.user_id not in (select m.user_id from measurements m where m.created_by_id = " + createdById + " and m.deleted=false) ";
        try (Connection con = DriverManager.getConnection(Constants.DB_DESTINATION_URL, Constants.DB_DESTINATION_USER_NAME, Constants.DB_DESTINATION_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                userCount = rs.getLong(1);
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return userCount;
    }
}
