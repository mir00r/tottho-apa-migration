package com.cmed.services;

import com.cmed.models.LoginResponseDto;
import com.cmed.models.UserIdInfo;
import com.cmed.services.login.LoginService;
import com.cmed.services.login.LoginServiceImpl;
import com.cmed.services.measurements.MeasurementPostService;
import com.cmed.services.measurements.MeasurementPostServiceImpl;
import com.cmed.services.users.UserMigrationService;
import com.cmed.services.users.UserMigrationServiceImpl;
import com.cmed.utils.Constants;

import java.io.IOException;
import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class MigrationServiceImpl implements MigrationService {

    private final LoginService loginService;
    private final MeasurementPostService measurementPostService;
    private final UserMigrationService userMigrationService;

    public MigrationServiceImpl() {
        this.loginService = new LoginServiceImpl();
        this.userMigrationService = new UserMigrationServiceImpl();
        this.measurementPostService = new MeasurementPostServiceImpl();
    }

    @Override
    public void execute(String companyCode) {
        try {
            // Get login response from destination server
            LoginResponseDto loginResponse = this.loginService.postLoginInDestination(null, Constants.DESTINATION_USERNAME, Constants.DESTINATION_PASSWORD);
            String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();

            // Get source and destination company user id list after successfully posting user into destination
            List<UserIdInfo> userIdList = this.userMigrationService.execute(companyCode);
            this.measurementPostService.processMeasurement(token, userIdList);
        } catch (IOException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

}
