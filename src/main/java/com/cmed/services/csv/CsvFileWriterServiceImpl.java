package com.cmed.services.csv;

import com.cmed.enums.BloodGroup;
import com.cmed.enums.Gender;
import com.cmed.models.BundleDto;
import com.cmed.models.EmployeeDto;
import com.cmed.models.UserData;
import com.cmed.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVWriter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * @author mir00r on 2/11/20
 * @project IntelliJ IDEA
 */
public class CsvFileWriterServiceImpl implements CsvFileWriterService {

    @Override
    public String writeUserRequestToCSVFile(List<UserData> requestData) throws IOException {
        String fileName = "employee_request.csv";
        CSVWriter writer = new CSVWriter(new FileWriter(fileName));
        String[] fields = {"username", "employee_user_id", "birthday", "first_name", "last_name", "gender", "blood_group", "phone", "diabetic", "height", "weight", "company_id", "company_employee_id"};
        writer.writeNext(fields);

        for (UserData uData : requestData) {
            String[] data = new String[fields.length];
            data[0] = uData.getCmedId();
            data[1] = String.valueOf(uData.getUserId());
            data[2] = String.valueOf(uData.getBirthday());
            data[3] = uData.getFirstName();
            data[4] = uData.getLastName();
            data[5] = Gender.getValue(uData.getGender());
            data[6] = BloodGroup.getValue(uData.getBloodGroup());
            data[7] = uData.getPhone();
            data[8] = "false";
            data[9] = String.valueOf(uData.getHeight());
            data[10] = String.valueOf(uData.getWeight());
            data[11] = String.valueOf(uData.getCompanyId());
            data[12] = uData.getCmedId();
            writer.writeNext(data);
        }
        writer.close();
        Utility.buildFile(fileName);
        return fileName;
    }

    @Override
    public void writeEmployeeRequestToCSVFile(List<EmployeeDto> requestData) throws IOException {
        String fileName = "employee_request.csv";
        String[] fields = {"username", "birthday", "first_name", "last_name", "gender", "blood_group", "phone", "diabetic", "height", "weight", "company_id", "company_employee_id"};
        CSVWriter writer = Utility.getCSVWriter(fileName, fields);

        for (EmployeeDto uData : requestData) {
            String[] data = new String[fields.length];
            data[0] = uData.getUsername();
            data[1] = String.valueOf(uData.getBirthday());
            data[2] = uData.getFirstNameEn();
            data[3] = uData.getLastNameEn();
            data[4] = Gender.getValue(uData.getGender());
            data[5] = BloodGroup.getValue(uData.getBloodGroup());
            data[6] = uData.getPhone();
            data[7] = "false";
            data[8] = String.valueOf(uData.getHeight());
            data[9] = String.valueOf(uData.getWeight());
            data[10] = String.valueOf(uData.getCompanyId());
            data[11] = uData.getCompanyEmployeeId();
            writer.writeNext(data);
        }
        writer.close();
        Utility.buildFile(fileName);
    }

    @Override
    public void writeUserResponseToCSVFile(List<JSONObject> responseData) throws IOException {

        String validFileName = "employee_valid_response.csv";
        String[] validFields = {"username", "employee_user_id", "birthday", "first_name", "last_name", "gender", "blood_group", "phone", "diabetic", "height", "weight", "company_id", "company_employee_id", "id"};
        CSVWriter validWriter = Utility.getCSVWriter(validFileName, validFields);

        String invalidFileName = "employee_invalid_response.csv";
        String[] invalidFields = {"username", "birthday", "first_name", "last_name", "gender", "blood_group", "phone", "diabetic", "height", "weight", "company_id", "company_employee_id"};
        CSVWriter invalidWriter = Utility.getCSVWriter(invalidFileName, invalidFields);

        for (JSONObject bData : responseData) {

            JSONArray employeeResponse;
            try {
                if (bData.getJSONArray("valid_list").length() > 0) {
                    employeeResponse = bData.getJSONArray("valid_list");

                    for (int i = 0; i < employeeResponse.length(); i++) {
                        String[] validData = new String[validFields.length];
                        JSONObject uData = employeeResponse.getJSONObject(i);

                        validData[0] = uData.getString("username");
                        validData[1] = String.valueOf(uData.getLong("user_id"));
                        validData[2] = String.valueOf(uData.get("birthday"));
                        validData[3] = uData.getString("first_name_en");
                        validData[4] = uData.getString("last_name_en");
                        validData[5] = Gender.getValue(Byte.parseByte(String.valueOf(uData.get("gender"))));
                        validData[6] = BloodGroup.getValue(Byte.parseByte(String.valueOf(uData.get("blood_group"))));
                        validData[7] = uData.getString("phone");
                        validData[8] = "false";
                        validData[9] = String.valueOf(uData.getDouble("height"));
                        validData[10] = String.valueOf(uData.getDouble("weight"));
                        validData[11] = String.valueOf(uData.getLong("company_id"));
                        validData[12] = uData.getString("company_employee_id");
                        validData[13] = String.valueOf(uData.getLong("id"));

                        validWriter.writeNext(validData);
                    }
                }
                if (bData.getJSONArray("invalid_list").length() > 0) {
                    employeeResponse = bData.getJSONArray("invalid_list");

                    for (int i = 0; i < employeeResponse.length(); i++) {
                        String[] invalidData = new String[invalidFields.length];
                        JSONObject uData = employeeResponse.getJSONObject(i);

                        invalidData[0] = uData.getString("username");
                        invalidData[1] = String.valueOf(uData.get("birthday"));
                        invalidData[2] = uData.getString("first_name_en");
                        invalidData[3] = uData.getString("last_name_en");
                        invalidData[4] = Gender.getValue(Byte.parseByte(String.valueOf(uData.get("gender"))));
                        invalidData[5] = BloodGroup.getValue(Byte.parseByte(String.valueOf(uData.get("blood_group"))));
                        invalidData[6] = uData.getString("phone");
                        invalidData[7] = "false";
                        invalidData[8] = String.valueOf(uData.getDouble("height"));
                        invalidData[9] = String.valueOf(uData.getDouble("weight"));
                        invalidData[10] = String.valueOf(uData.getLong("company_id"));
                        invalidData[11] = uData.getString("company_employee_id");

                        invalidWriter.writeNext(invalidData);
                    }
                }
            } catch (JSONException | NumberFormatException e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
        validWriter.close();
        invalidWriter.close();
        Utility.buildFile(validFileName);
        Utility.buildFile(invalidFileName);
    }

    @Override
    public void writeBundleRequestToCSVFile(List<BundleDto> requestData) throws IOException {
        String fileName = "bundle_request.csv";
        String[] fields = {"bundle_user_id", "bundle_measurement"};
        CSVWriter writer = Utility.getCSVWriter(fileName, fields);

        for (BundleDto bData : requestData) {
            String[] data = new String[fields.length];
            ObjectMapper mapper = new ObjectMapper();

            data[0] = String.valueOf(bData.getUserId());
            data[1] = mapper.writeValueAsString(bData.getBundleMeasurement());

            writer.writeNext(data);
        }
        writer.close();
        Utility.buildFile(fileName);
    }

    @Override
    public void writeBundleResponseToCSVFile(List<JSONObject> responseData) throws IOException {
        String validFileName = "bundle_valid_response.csv";
        String[] validFields = {"measurement_id", "bundle_id", "user_id", "inputs", "measured_at", "measurement_type_code", "measurement_type_code_id", "tag"};
        CSVWriter validWriter = Utility.getCSVWriter(validFileName, validFields);

        String invalidFileName = "bundle_invalid_response.csv";
        String[] invalidFields = {"user_id", "inputs", "measured_at", "measurement_type_code_id", "tag"};
        CSVWriter invalidWriter = Utility.getCSVWriter(invalidFileName, invalidFields);

        for (JSONObject responseDatum : responseData) {
            JSONArray measurementArray;

            try {
                if (responseDatum.getJSONArray("valid_list").length() > 0) {
                    for (int j = 0; j < responseDatum.getJSONArray("valid_list").length(); j++) {
                        measurementArray = responseDatum.getJSONArray("valid_list").getJSONObject(j).getJSONArray("bundles_measurements");

                        for (int k = 0; k < measurementArray.length(); k++) {
                            String[] validData = new String[validFields.length];
                            JSONObject measurement = measurementArray.getJSONObject(k);

                            validData[0] = String.valueOf(measurement.getLong("id"));
                            validData[1] = String.valueOf(measurement.getLong("bundle_id"));
                            validData[2] = String.valueOf(measurement.getLong("user_id"));
                            validData[3] = measurement.getJSONObject("inputs").toString();
                            validData[4] = String.valueOf(measurement.getLong("measured_at"));
                            validData[5] = measurement.getString("measurement_type_code");
                            validData[6] = String.valueOf(measurement.getInt("measurement_type_code_id"));
                            if (measurement.isNull("tag")) validData[7] = "";
                            else validData[7] = measurement.getString("tag");

                            validWriter.writeNext(validData);
                        }
                    }
                }
                if (responseDatum.getJSONArray("invalid_list").length() > 0) {
                    for (int j = 0; j < responseDatum.getJSONArray("invalid_list").length(); j++) {
                        measurementArray = responseDatum.getJSONArray("invalid_list").getJSONObject(j).getJSONArray("bundles_measurements");

                        for (int k = 0; k < measurementArray.length(); k++) {
                            String[] invalidData = new String[invalidFields.length];
                            JSONObject measurement = measurementArray.getJSONObject(k);

                            invalidData[0] = String.valueOf(measurement.getLong("user_id"));
                            invalidData[1] = measurement.getJSONObject("inputs").toString();
                            invalidData[2] = String.valueOf(measurement.getLong("measured_at"));
                            invalidData[3] = String.valueOf(measurement.getInt("measurement_type_code_id"));
                            if (measurement.isNull("tag")) invalidData[4] = "";
                            else invalidData[4] = measurement.getString("tag");

                            invalidWriter.writeNext(invalidData);
                        }
                    }
                }
            } catch (JSONException e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
        validWriter.close();
        invalidWriter.close();
        Utility.buildFile(validFileName);
        Utility.buildFile(invalidFileName);
    }

    @Override
    public void writeBundleResponseToCSVFile(JSONObject responseData) throws IOException {
        String validFileName = "bundle_valid_response.csv";
        String[] validFields = {"measurement_id", "bundle_id", "user_id", "inputs", "measured_at", "measurement_type_code", "measurement_type_code_id", "tag"};
        CSVWriter validWriter = Utility.getCSVWriter(validFileName, validFields);

        String invalidFileName = "bundle_invalid_response.csv";
        String[] invalidFields = {"user_id", "inputs", "measured_at", "measurement_type_code_id", "tag", "message"};
        CSVWriter invalidWriter = Utility.getCSVWriter(invalidFileName, invalidFields);

        JSONArray measurementArray;
        try {
            if (responseData.getJSONArray("valid_list").length() > 0) {
                for (int j = 0; j < responseData.getJSONArray("valid_list").length(); j++) {
                    measurementArray = responseData.getJSONArray("valid_list").getJSONObject(j).getJSONArray("bundles_measurements");

                    for (int k = 0; k < measurementArray.length(); k++) {
                        String[] validData = new String[validFields.length];
                        JSONObject measurement = measurementArray.getJSONObject(k);

                        validData[0] = String.valueOf(measurement.getLong("id"));
                        validData[1] = String.valueOf(measurement.getLong("bundle_id"));
                        validData[2] = String.valueOf(measurement.getLong("user_id"));
                        validData[3] = measurement.getJSONObject("inputs").toString();
                        validData[4] = String.valueOf(measurement.getLong("measured_at"));
                        validData[5] = measurement.getString("measurement_type_code");
                        validData[6] = String.valueOf(measurement.getInt("measurement_type_code_id"));
                        if (measurement.isNull("tag")) validData[7] = "";
                        else validData[7] = measurement.getString("tag");

                        validWriter.writeNext(validData);
                    }
                }
            }
            if (responseData.getJSONArray("invalid_list").length() > 0) {
                for (int j = 0; j < responseData.getJSONArray("invalid_list").length(); j++) {
                    measurementArray = responseData.getJSONArray("invalid_list").getJSONObject(j).getJSONArray("bundles_measurements");

                    for (int k = 0; k < measurementArray.length(); k++) {
                        String[] invalidData = new String[invalidFields.length];
                        JSONObject measurement = measurementArray.getJSONObject(k);

                        invalidData[0] = String.valueOf(measurement.getLong("user_id"));
                        invalidData[1] = measurement.getJSONObject("inputs").toString();
                        invalidData[2] = String.valueOf(measurement.getLong("measured_at"));
                        invalidData[3] = String.valueOf(measurement.getInt("measurement_type_code_id"));
                        if (measurement.isNull("tag")) invalidData[4] = "";
                        else invalidData[4] = measurement.getString("tag");

                        invalidWriter.writeNext(invalidData);
                    }
                }
            } else {
                String[] errorData = new String[invalidFields.length];
                ObjectMapper mapper = new ObjectMapper();
                errorData[5] = mapper.writeValueAsString(responseData);
            }
        } catch (JSONException e) {
            System.out.println(e.getLocalizedMessage());
        }

        validWriter.close();
        invalidWriter.close();
        Utility.buildFile(validFileName);
        Utility.buildFile(invalidFileName);
    }
}
