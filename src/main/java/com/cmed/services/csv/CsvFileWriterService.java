package com.cmed.services.csv;

import com.cmed.models.BundleDto;
import com.cmed.models.EmployeeDto;
import com.cmed.models.UserData;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * @author mir00r on 2/11/20
 * @project IntelliJ IDEA
 */
public interface CsvFileWriterService {
    String writeUserRequestToCSVFile(List<UserData> requestData) throws IOException;

    void writeEmployeeRequestToCSVFile(List<EmployeeDto> requestData) throws IOException;

    void writeUserResponseToCSVFile(List<JSONObject> responseData) throws IOException;

    void writeBundleRequestToCSVFile(List<BundleDto> requestData) throws IOException;

    void writeBundleResponseToCSVFile(List<JSONObject> responseData) throws IOException;

    void writeBundleResponseToCSVFile(JSONObject responseData) throws IOException;
}
