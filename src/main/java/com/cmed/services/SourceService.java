package com.cmed.services;

import com.cmed.Main;
import com.cmed.enums.BloodGroup;
import com.cmed.enums.Gender;
import com.cmed.enums.MeasurementTags;
import com.cmed.models.*;
import com.cmed.utils.Constants;
import com.cmed.utils.NetworkUtil;
import com.cmed.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mir00r on 27/10/20
 * @project IntelliJ IDEA
 */
public class SourceService {

    public static List<UserData> getUserSourceData(String offset) {
        List<UserData> sourceData = new ArrayList<>();

        String userQuery = "select u.cmed_id, u.company_id, u.customer_id from user u where company_id = (SELECT c.id FROM company c WHERE code = 'TotthoApa') and cmed_id not like 'tsk%' limit " + Constants.LIMIT + " offset " + offset;
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(userQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                sourceData.add(new UserData(
                        rs.getString(1),
                        rs.getLong(2), rs.getLong(3)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return sourceData;
    }

    public static List<MeasurementData> getMeasurementSourceData(Long personId) {
        List<MeasurementData> measurementData = new ArrayList<>();

        String measurementQuery = "SELECT um.measured_at, um.measurement_type_id, um.value1, um.value2, um.value3, u.cmed_id, p.birthday, p.gender, p.blood_group, p.first_name, p.last_name, p.height, p.weight, p.phone_number, mt.tag_id FROM user_measurement um join user u on um.person_id = u.customer_id join person p on u.customer_id = p.id left join measurement_tags mt on um.id = mt.measurement_id WHERE um.person_id = " + personId + " ORDER BY um.created_at DESC ";
        try (Connection con = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER_NAME, Constants.DB_PASSWORD);
             PreparedStatement userPst = con.prepareStatement(measurementQuery);
             ResultSet rs = userPst.executeQuery()) {

            while (rs.next()) {
                measurementData.add(new MeasurementData(
                        rs.getTimestamp(1).getTime(),
                        rs.getLong(2), rs.getDouble(3),
                        rs.getDouble(4), rs.getDouble(5),
                        rs.getString(6), rs.getTimestamp(7).getTime(),
                        rs.getString(8), rs.getString(9),
                        rs.getString(10), rs.getString(11),
                        rs.getDouble(12), rs.getDouble(13),
                        rs.getString(14), rs.getLong(15)));
            }
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Main.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return measurementData;
    }

    public static MeasurementDto prepareMeasurementData(MeasurementData data) {
        MeasurementDto dto = new MeasurementDto();

        dto.setMeasuredAt(data.getMeasuredAt());
        dto.setMeasurementTypeCodeId(data.getMeasurementTypeId().byteValue());
        dto.setUserId(data.getUserId());

        if (data.getMeasurementTypeId() == 1) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("SYSTOLIC", data.getValue1());
            input.put("DIASTOLIC", data.getValue2());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 2) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("TEMP", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 3) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("WEIGHT", data.getValue1());
            input.put("HEIGHT", data.getValue2());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 4) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("SUGAR", data.getValue1());
            dto.setInputs(input);
            dto.setTag(MeasurementTags.getName(data.getTagId().byteValue()));
        } else if (data.getMeasurementTypeId() == 5) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("PULSE_RATE", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 6) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("RESPIRATION_RATE", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 7) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("ECG", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 8) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("SPO2", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 9) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("HEIGHT", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 10) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("MUAC", data.getValue1());
            dto.setInputs(input);
        }
        return dto;
    }

    public static BundleDto prepareBundleData(List<MeasurementDto> measurementData, Long userId) {
        BundleDto dto = new BundleDto();
        dto.setBundleMeasurement(measurementData);
        dto.setUserId(userId);
        return dto;
    }

    public static Long getUserId(MeasurementData data, LoginResponseDto loginResponse) {
        String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();
        String username = Utility.getFormattedPhoneNumber(data.getCmedId());
        try {
            String userResponse = NetworkUtil.getResponseFromGETRequest(Constants.getUserUrlByUserName(username), token);
            String agentMe = NetworkUtil.getResponseFromGETRequest(Constants.getAgentMeUrl(), token);

            if (userResponse.startsWith("{")) {
                JSONObject obj = new JSONObject(userResponse);
                return obj.getLong("id");
            } else {
                ObjectMapper mapper = new ObjectMapper();
                EmployeeDto dto = new EmployeeDto();
                dto.setBirthday(data.getBirthday());
                dto.setUsername(username);
                dto.setCompanyEmployeeId(data.getCmedId());

                JSONObject agentObj = new JSONObject(agentMe);
                if (agentMe.startsWith("{")) {
                    dto.setCompanyId(agentObj.getLong("id"));
                } else dto.setCompanyId(11L);

                dto.setHeight(data.getHeight());
                dto.setWeight(data.getWeight());
                dto.setFirstNameEn(data.getFirstName());
                dto.setLastNameEn(data.getLastName());
                dto.setBloodGroup(BloodGroup.getKey(data.getBloodGroup()));
                dto.setGender(Gender.getKey(data.getGender()));
                dto.setDiabetic(false);
                dto.setPhone(data.getPhone());

                String employeeResponse = NetworkUtil.getResponseFromPostData(Constants.getEmployeePostUrl(), mapper.writeValueAsString(dto), token);
                JSONObject employeeObj = new JSONObject(employeeResponse);
                return employeeObj.getLong("user_id");
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        return 0L;
    }

    public static void execute(LoginResponseDto loginResponse) {
        try {
            int offset = 0;
            while (true) {
                List<UserData> userData = SourceService.getUserSourceData(String.valueOf(offset));
                if (userData.size() == 0) {
                    System.out.println("Source data empty so break the programme");
                    break;
                }
                offset += userData.size();

                for (UserData udata : userData) {
                    List<MeasurementData> measurementData = getMeasurementSourceData(udata.getCustomerId());
                    List<BundleDto> bundleDtoList = new ArrayList<>();

                    if (measurementData.size() > 0) {
                        Long userId = getUserId(measurementData.get(0), loginResponse);
                        List<MeasurementDto> measurementDtoList = new ArrayList<>();

                        // Prepare measurement data
                        for (MeasurementData mData : measurementData) {
                            mData.setUserId(userId);
                            measurementDtoList.add(SourceService.prepareMeasurementData(mData));
                        }
                        // Prepare bundle data
                        bundleDtoList.add(prepareBundleData(measurementDtoList, userId));

                        ObjectMapper mapper = new ObjectMapper();
                        String bundleJson = mapper.writeValueAsString(bundleDtoList);

                        // Post destination database through API
                        String bundleResponse = NetworkUtil.getResponseFromPostData(Constants.getBulkBundlesPostUrl(), bundleJson, loginResponse.getTokenType() + " " + loginResponse.getAccessToken());
                        System.out.println("Measurement data posted: " + bundleResponse);
                    } else System.out.println("Measurement data not found with person id: " + udata.getCustomerId());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void printResultSet(ResultSet rs) throws SQLException {
        System.out.println("----------------------------------------------------------------------");
        System.out.println("Measure At: " + rs.getDate(1));
        System.out.println("Measurement Type Id: " + rs.getLong(2));
        System.out.println("Value 1: " + rs.getDouble(3));
        System.out.println("Value 2: " + rs.getString(4));
        System.out.println("Value 3: " + rs.getString(5));
        System.out.println("CMED Id: " + rs.getString(6));
        System.out.println("----------------------------------------------------------------------");
    }
}
