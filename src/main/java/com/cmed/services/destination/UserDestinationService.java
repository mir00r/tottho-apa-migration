package com.cmed.services.destination;

import com.cmed.models.LoginResponseDto;
import com.cmed.models.UserData;
import com.cmed.models.UserIdInfo;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface UserDestinationService {
    Long getUserIdFromDestination(UserData data, LoginResponseDto loginResponse);
    JSONObject postUserToDestination(UserData data, LoginResponseDto loginResponse, Long companyId);
    List<UserIdInfo> postBulkUserToDestination(List<UserData> data, LoginResponseDto loginResponse, Long companyId) throws IOException;
}
