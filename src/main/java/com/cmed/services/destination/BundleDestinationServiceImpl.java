package com.cmed.services.destination;

import com.cmed.models.BundleDto;
import com.cmed.models.MeasurementDto;

import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class BundleDestinationServiceImpl implements BundleDestinationService{
    @Override
    public BundleDto prepareBundleData(List<MeasurementDto> measurementData, Long userId) {
        BundleDto dto = new BundleDto();
        dto.setBundleMeasurement(measurementData);
        dto.setUserId(userId);
        return dto;
    }
}
