package com.cmed.services.destination;

import com.cmed.enums.MeasurementTags;
import com.cmed.models.MeasurementData;
import com.cmed.models.MeasurementDto;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class MeasurementDestinationServiceImpl implements MeasurementDestinationService {
    @Override
    public MeasurementDto prepareMeasurementData(MeasurementData data) {
        MeasurementDto dto = new MeasurementDto();

        dto.setMeasuredAt(data.getMeasuredAt());
        dto.setMeasurementTypeCodeId(data.getMeasurementTypeId().byteValue());
        dto.setUserId(data.getUserId());

        if (data.getMeasurementTypeId() == 1) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("SYSTOLIC", data.getValue1());
            input.put("DIASTOLIC", data.getValue2());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 2) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("TEMP", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 3) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("WEIGHT", data.getValue1());
            input.put("HEIGHT", data.getValue2());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 4) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("SUGAR", data.getValue1());
            dto.setInputs(input);
            dto.setTag(MeasurementTags.getName(data.getTagId().byteValue()));
        } else if (data.getMeasurementTypeId() == 5) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("PULSE_RATE", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 6) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("RESPIRATION_RATE", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 7) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("ECG", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 8) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("SPO2", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 9) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("HEIGHT", data.getValue1());
            dto.setInputs(input);
        } else if (data.getMeasurementTypeId() == 10) {
            Map<String, Double> input = new LinkedHashMap<>();
            input.put("MUAC", data.getValue1());
            dto.setInputs(input);
        }
        return dto;
    }
}
