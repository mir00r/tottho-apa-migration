package com.cmed.services.destination;

import com.cmed.enums.BloodGroup;
import com.cmed.enums.Gender;
import com.cmed.models.EmployeeDto;
import com.cmed.models.LoginResponseDto;
import com.cmed.models.UserData;
import com.cmed.models.UserIdInfo;
import com.cmed.services.csv.CsvFileWriterService;
import com.cmed.services.csv.CsvFileWriterServiceImpl;
import com.cmed.utils.Constants;
import com.cmed.utils.NetworkUtil;
import com.cmed.utils.Utility;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public class UserDestinationServiceImpl implements UserDestinationService {

    private final CsvFileWriterService csvFileWriterService;

    public UserDestinationServiceImpl() {
        this.csvFileWriterService = new CsvFileWriterServiceImpl();
    }

    @Override
    public Long getUserIdFromDestination(UserData data, LoginResponseDto loginResponse) {
        String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();
        String username = Utility.getFormattedPhoneNumber(data.getCmedId());
        try {
            String userResponse = NetworkUtil.getResponseFromGETRequest(Constants.getUserUrlByUserName(username), token);
            String agentMe = NetworkUtil.getResponseFromGETRequest(Constants.getAgentMeUrl(), token);

            if (userResponse.startsWith("{")) {
                JSONObject obj = new JSONObject(userResponse);
                return obj.getLong("id");
            } else {
                ObjectMapper mapper = new ObjectMapper();
                EmployeeDto dto = new EmployeeDto();
                dto.setBirthday(data.getBirthday());
                dto.setUsername(username);
                dto.setCompanyEmployeeId(data.getCmedId());

                JSONObject agentObj = new JSONObject(agentMe);
                if (agentMe.startsWith("{")) {
                    dto.setCompanyId(agentObj.getLong("id"));
                } else dto.setCompanyId(11L);

                dto.setHeight(data.getHeight());
                dto.setWeight(data.getWeight());
                dto.setFirstNameEn(data.getFirstName());
                dto.setLastNameEn(data.getLastName());
                dto.setBloodGroup(BloodGroup.getKey(data.getBloodGroup()));
                dto.setGender(Gender.getKey(data.getGender()));
                dto.setDiabetic(false);
                dto.setPhone(data.getPhone());

                String employeeResponse = NetworkUtil.getResponseFromPostData(Constants.getEmployeePostUrl(), mapper.writeValueAsString(dto), token);
                JSONObject employeeObj = new JSONObject(employeeResponse);
                return employeeObj.getLong("user_id");
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        return 0L;
    }

    @Override
    public JSONObject postUserToDestination(UserData data, LoginResponseDto loginResponse, Long companyId) {
        String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();
        String username = Utility.getFormattedPhoneNumber(data.getCmedId());
        try {
            EmployeeDto dto = new EmployeeDto();
            dto.setBirthday(data.getBirthday());
            dto.setUsername(username);
            dto.setCompanyEmployeeId(data.getCmedId());
            dto.setCompanyId(companyId);
            dto.setHeight(data.getHeight());
            dto.setWeight(data.getWeight());
            dto.setFirstNameEn(data.getFirstName());
            dto.setLastNameEn(data.getLastName());
            dto.setBloodGroup(BloodGroup.getKey(data.getBloodGroup()));
            dto.setGender(Gender.getKey(data.getGender()));
            dto.setDiabetic(false);
            dto.setPhone(data.getPhone());

            ObjectMapper mapper = new ObjectMapper();
            String requestJson = mapper.writeValueAsString(dto);
            String employeeResponse = NetworkUtil.getResponseFromPostData(Constants.getEmployeePostUrl(), requestJson, token);

            if (employeeResponse.startsWith("{")) return new JSONObject(employeeResponse);
            else {
                JSONObject jsonObject = new JSONObject();
                return jsonObject.put("error", employeeResponse);
            }
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        return null;
    }

    @Override
    public List<UserIdInfo> postBulkUserToDestination(List<UserData> userData, LoginResponseDto loginResponse, Long companyId) throws IOException {
        String token = loginResponse.getTokenType() + " " + loginResponse.getAccessToken();
        List<UserIdInfo> responseUserIds = new ArrayList<>();
        List<EmployeeDto> request = new ArrayList<>();
        List<JSONObject> response = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        Long sourceCompanyId = 0L;

        for (UserData uData : userData) {
            String username = Utility.getFormattedPhoneNumber(uData.getCmedId());
            sourceCompanyId = uData.getCompanyId();

            EmployeeDto dto = new EmployeeDto();
            dto.setBirthday(uData.getBirthday());

            if (Utility.isUserNameValid(username.trim()))
                dto.setUsername(username.trim());
            else dto.setUsername(Utility.getFormattedPhoneNumber(uData.getPhone().trim()));

            dto.setCompanyEmployeeId(String.valueOf(uData.getCustomerId()));
            dto.setCompanyId(companyId);
            dto.setHeight(uData.getHeight());
            dto.setWeight(uData.getWeight());
            dto.setFirstNameEn(uData.getFirstName());
            dto.setLastNameEn(uData.getLastName());
            dto.setBloodGroup(BloodGroup.getKey(uData.getBloodGroup()));
            dto.setGender(Gender.getKey(uData.getGender()));
            dto.setDiabetic(false);
            dto.setPhone(uData.getPhone());

            request.add(dto);
        }
        try {
            String requestJson = mapper.writeValueAsString(request);
            String bulkResponse = NetworkUtil.getResponseFromPostData(Constants.getEmployeeBulkPostUrl(), requestJson, token);
            if (bulkResponse.startsWith("{"))
                response.add(new JSONObject(bulkResponse));
            else response.add(new JSONObject().put("error", bulkResponse));

            // Get user or employee id information after posting the data to destination
            responseUserIds = this.getUserInfo(bulkResponse, sourceCompanyId);

        } catch (Exception exception) {
            System.out.println(exception.getLocalizedMessage());
        } finally {
            // Write request into CSV file
            this.csvFileWriterService.writeEmployeeRequestToCSVFile(request);
            // Write response into CSV file
            this.csvFileWriterService.writeUserResponseToCSVFile(response);
        }
        return responseUserIds;
    }

    private List<UserIdInfo> getUserInfo(String bulkResponse, Long companyId) {
        List<UserIdInfo> responseUserIds = new ArrayList<>();
        JSONObject jsonResponse = new JSONObject(bulkResponse);
        JSONArray validList;
        if (jsonResponse.getJSONArray("valid_list").length() > 0) {
            validList = jsonResponse.getJSONArray("valid_list");
            for (int i = 0; i < validList.length(); i++) {
                JSONObject employeeResponse = validList.getJSONObject(i);
                responseUserIds.add(new UserIdInfo(Long.parseLong(employeeResponse.getString("company_employee_id")), employeeResponse.getLong("user_id"), companyId));
            }
        }
        return responseUserIds;
    }

}
