package com.cmed.services.destination;

import com.cmed.models.BundleDto;
import com.cmed.models.MeasurementDto;

import java.util.List;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface BundleDestinationService {
    BundleDto prepareBundleData(List<MeasurementDto> measurementData, Long userId);
}
