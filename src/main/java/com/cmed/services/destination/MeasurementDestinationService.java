package com.cmed.services.destination;

import com.cmed.models.MeasurementData;
import com.cmed.models.MeasurementDto;

/**
 * @author mir00r on 31/10/20
 * @project IntelliJ IDEA
 */
public interface MeasurementDestinationService {
    MeasurementDto prepareMeasurementData(MeasurementData data);
}
